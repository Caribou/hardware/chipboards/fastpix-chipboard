Altium Designer Pick and Place Locations
C:\Users\ebuschma\AppData\Local\TempReleases\Snapshot\3\Assembly flip-chip\Pick Place\Pick Place for fastpix_chipboard(flip-chip).txt

========================================================================================================================
File Design Information:

Date:       03/06/21
Time:       10:18
Revision:   Not in VersionControl
Variant:    flip-chip
Units used: mil

Center-X(mil) Center-Y(mil) Comment                        Description               Designator Footprint                      Layer       Rotation 
944.882       826.772       20k                            "Resistor - 0.1%"         R67        RESC1005X40N                   TopLayer    360      
3543.307      1292.323      0                              Resistor                  R59        RESC1005X40N                   BottomLayer 90       
3543.307      1382.874      0                              Resistor                  R51        RESC1005X40N                   BottomLayer 270      
3543.307      2565.945      0                              Resistor                  R50        RESC1005X40N                   BottomLayer 270      
3543.307      2473.425      0                              Resistor                  R49        RESC1005X40N                   BottomLayer 90       
393.701       2563.927      0                              Resistor                  R48        RESC1005X40N                   BottomLayer 270      
393.701       2470.669      0                              Resistor                  R18        RESC1005X40N                   BottomLayer 90       
1129.193      907.677       0                              Resistor                  R10        RESC1005X40N                   TopLayer    90       
1016.693      926.673       MCP6V86                        ""                        U6         SOT95P270X145-5N               TopLayer    360      
1048.976      1026.673      100nF                          "Non-polarised capacitor" C16        CAPC1005X55N                   TopLayer    360      
1035.197      1089.173      1uF                            "Non-polarised capacitor" C15        CAPC1608X87N                   TopLayer    360      
2259.409      1064.173      100nF                          "Non-polarised capacitor" C14        CAPC1005X55N                   TopLayer    360      
2559.409      1064.173      100nF                          "Non-polarised capacitor" C13        CAPC1005X55N                   TopLayer    360      
2409.409      1064.173      100nF                          "Non-polarised capacitor" C12        CAPC1005X55N                   TopLayer    360      
2709.409      1064.173      100nF                          "Non-polarised capacitor" C11        CAPC1005X55N                   TopLayer    360      
2688.976      956.693       1uF                            "Non-polarised capacitor" C10        CAPC1608X87N                   TopLayer    360      
2241.693      1176.673      MCP6V86                        ""                        U5         SOT95P270X145-5N               TopLayer    270      
2391.693      1176.673      MCP6V86                        ""                        U4         SOT95P270X145-5N               TopLayer    270      
2541.693      1176.673      MCP6V86                        ""                        U3         SOT95P270X145-5N               TopLayer    270      
2691.693      1176.673      MCP6V86                        ""                        U2         SOT95P270X145-5N               TopLayer    270      
2286.417      3389.764      10nF                           "Non-polarised capacitor" C37        CAPC1005X55N                   TopLayer    180      
1579.193      1170.669      0                              Resistor                  R7         RESC1005X40N                   TopLayer    90       
2279.528      3476.378      10nF                           "Non-polarised capacitor" C38        CAPC1005X55N                   TopLayer    180      
1669.291      3281.496      10uF                           "Non-polarised capacitor" C34        CAPC2012X135N                  TopLayer    270      
2328.740      3299.213      10uF                           "Non-polarised capacitor" C33        CAPC2012X135N                  TopLayer    270      
2118.110      3690.945      10nF                           "Non-polarised capacitor" C35        CAPC1005X55N                   TopLayer    180      
1801.181      3769.685      10nF                           "Non-polarised capacitor" C31        CAPC1608X90N                   TopLayer    270      
2374.016      3566.929      10nF                           "Non-polarised capacitor" C36        CAPC1005X55N                   TopLayer    360      
2109.252      3770.669      10nF                           "Non-polarised capacitor" C32        CAPC1608X90N                   TopLayer    90       
2255.905      3295.276      10nF                           "Non-polarised capacitor" C42        CAPC1608X90N                   TopLayer    270      
1817.913      3689.961      10nF                           "Non-polarised capacitor" C26        CAPC1005X55N                   TopLayer    360      
1558.071      3525.591      10nF                           "Non-polarised capacitor" C30        CAPC1005X55N                   TopLayer    360      
1626.968      3746.063      10nF                           "Non-polarised capacitor" C22        CAPC1608X90N                   TopLayer    90       
2094.488      3247.047      10nF                           "Non-polarised capacitor" C39        CAPC1005X55N                   TopLayer    270      
1840.551      3769.685      10nF                           "Non-polarised capacitor" C29        CAPC1005X55N                   TopLayer    270      
1743.110      3294.291      10nF                           "Non-polarised capacitor" C23        CAPC1608X90N                   TopLayer    270      
1687.992      3731.299      10nF                           "Non-polarised capacitor" C24        CAPC1608X90N                   TopLayer    270      
1750.000      3731.299      10nF                           "Non-polarised capacitor" C25        CAPC1608X90N                   TopLayer    270      
1643.701      3425.197      10nF                           "Non-polarised capacitor" C27        CAPC1005X55N                   TopLayer    360      
1643.701      3464.567      10nF                           "Non-polarised capacitor" C28        CAPC1005X55N                   TopLayer    360      
2314.961      3743.110      10nF                           "Non-polarised capacitor" C41        CAPC1608X90N                   TopLayer    90       
2253.937      3743.110      10nF                           "Non-polarised capacitor" C43        CAPC1608X90N                   TopLayer    270      
2187.008      3743.110      10nF                           "Non-polarised capacitor" C40        CAPC1608X90N                   TopLayer    270      
3001.968      3248.032      10uF                           "Non-polarised capacitor" C44        CAPC2012X135N                  TopLayer    270      
2800.000      1554.134      10uF                           "Non-polarised capacitor" C21        CAPC2012X135N                  TopLayer    270      
2900.000      1554.134      10uF                           "Non-polarised capacitor" C20        CAPC2012X135N                  TopLayer    270      
1704.193      1070.669      20k                            "Resistor - 0.1%"         R28        RESC1005X40N                   BottomLayer 90       
1579.193      1070.669      2k                             "Resistor - 1%"           R22        RESC1005X40N                   BottomLayer 90       
1516.693      1070.669      0                              Resistor                  R20        RESC1005X40N                   BottomLayer 90       
1766.693      1070.669      200                            "Resistor - 1%"           R30        RESC1005X40N                   BottomLayer 90       
1829.193      1070.669      499                            "Resistor - 1%"           R37        RESC1005X40N                   BottomLayer 90       
1891.693      1070.669      2M                             "Resistor - 1%"           R39        RESC1005X40N                   BottomLayer 90       
1641.693      1070.669      20k                            "Resistor - 0.1%"         R25        RESC1005X40N                   BottomLayer 90       
1201.968      3762.795      51                             "Resistor - 1%"           R43_T      RESC1005X40N                   TopLayer    180      
2653.819      3762.795      51                             "Resistor - 1%"           R43_B      RESC1005X40N                   TopLayer    360      
1280.709      3733.268      51                             "Resistor - 1%"           R41_T      RESC1005X40N                   TopLayer    180      
2653.819      3703.740      51                             "Resistor - 1%"           R41_B      RESC1005X40N                   TopLayer    360      
1201.968      3703.740      51                             "Resistor - 1%"           R40_T      RESC1005X40N                   TopLayer    180      
2732.559      3733.268      51                             "Resistor - 1%"           R40_B      RESC1005X40N                   TopLayer    360      
1044.488      3674.213      51                             "Resistor - 1%"           R39_T      RESC1005X40N                   TopLayer    180      
2811.299      3674.213      51                             "Resistor - 1%"           R39_B      RESC1005X40N                   TopLayer    360      
1044.488      3615.157      51                             "Resistor - 1%"           R37_T      RESC1005X40N                   TopLayer    180      
2811.299      3615.157      51                             "Resistor - 1%"           R37_B      RESC1005X40N                   TopLayer    360      
1123.228      3644.685      51                             "Resistor - 1%"           R36_T      RESC1005X40N                   TopLayer    180      
2890.039      3644.685      51                             "Resistor - 1%"           R36_B      RESC1005X40N                   TopLayer    360      
1201.968      3585.630      51                             "Resistor - 1%"           R35_T      RESC1005X40N                   TopLayer    180      
2653.819      3585.630      51                             "Resistor - 1%"           R35_B      RESC1005X40N                   TopLayer    360      
1280.709      3556.102      51                             "Resistor - 1%"           R33_T      RESC1005X40N                   TopLayer    180      
2653.819      3526.575      51                             "Resistor - 1%"           R33_B      RESC1005X40N                   TopLayer    360      
1201.968      3526.575      51                             "Resistor - 1%"           R32_T      RESC1005X40N                   TopLayer    180      
2732.559      3556.102      51                             "Resistor - 1%"           R32_B      RESC1005X40N                   TopLayer    360      
1044.488      3497.047      51                             "Resistor - 1%"           R31_T      RESC1005X40N                   TopLayer    180      
2811.299      3497.047      51                             "Resistor - 1%"           R31_B      RESC1005X40N                   TopLayer    360      
1044.488      3437.992      51                             "Resistor - 1%"           R29_T      RESC1005X40N                   TopLayer    180      
2811.299      3437.992      51                             "Resistor - 1%"           R29_B      RESC1005X40N                   TopLayer    360      
1123.228      3467.520      51                             "Resistor - 1%"           R28_T      RESC1005X40N                   TopLayer    180      
2890.039      3467.520      51                             "Resistor - 1%"           R28_B      RESC1005X40N                   TopLayer    360      
3583.661      1382.874      51                             "Resistor - 1%"           R52        RESC1005X40N                   BottomLayer 270      
3582.677      2473.425      51                             "Resistor - 1%"           R35        RESC1005X40N                   BottomLayer 90       
353.297       2470.669      51                             "Resistor - 1%"           R9         RESC1005X40N                   BottomLayer 90       
3583.661      1292.323      51                             "Resistor - 1%"           R63        RESC1005X40N                   BottomLayer 90       
3582.677      2565.945      51                             "Resistor - 1%"           R44        RESC1005X40N                   BottomLayer 270      
353.297       2563.927      51                             "Resistor - 1%"           R14        RESC1005X40N                   BottomLayer 270      
1829.193      1170.669      499                            "Resistor - 1%"           R36        RESC1005X40N                   BottomLayer 270      
1704.193      1170.669      20k                            "Resistor - 0.1%"         R27        RESC1005X40N                   BottomLayer 270      
1241.693      1045.669      50                             "Resistor - 0.1%"         R26        RESC1005X35N                   TopLayer    90       
2191.693      1626.673      22k                            "Resistor - 1%"           R58        RESC1005X40N                   BottomLayer 315      
2779.193      1926.673      22k                            "Resistor - 1%"           R65        RESC1005X40N                   BottomLayer 180      
2124.803      1417.323      22k                            "Resistor - 1%"           R56        RESC1005X40N                   BottomLayer 360      
1816.693      564.173       22k                            "Resistor - 1%"           R66        RESC1005X40N                   TopLayer    0        
2370.630      1620.079      22k                            "Resistor - 1%"           R54        RESC1005X40N                   TopLayer    315      
2233.071      1417.323      22k                            "Resistor - 1%"           R45        RESC1005X40N                   BottomLayer 180      
2707.874      2007.874      22k                            "Resistor - 1%"           R64        RESC1005X40N                   BottomLayer 360      
1447.835      903.543       22k                            "Resistor - 1%"           R55        RESC1005X40N                   BottomLayer 360      
2479.193      1614.173      22k                            "Resistor - 1%"           R57        RESC1005X40N                   TopLayer    135      
2566.693      1951.673      22k                            "Resistor - 1%"           R53        RESC1005X40N                   BottomLayer 180      
2194.882      1820.866      22k                            "Resistor - 1%"           R47        RESC1005X40N                   TopLayer    360      
1891.693      1170.669      2M                             "Resistor - 1%"           R38        RESC1005X40N                   BottomLayer 270      
1766.693      1170.669      200                            "Resistor - 1%"           R29        RESC1005X40N                   BottomLayer 270      
1641.693      1170.669      20k                            "Resistor - 0.1%"         R24        RESC1005X40N                   BottomLayer 270      
1954.193      1070.669      200                            "Resistor - 1%"           R23        RESC1005X40N                   BottomLayer 270      
1579.193      1170.669      2k                             "Resistor - 0.1%"         R21        RESC1005X40N                   BottomLayer 270      
1516.693      1170.669      20k                            "Resistor - 0.1%"         R19        RESC1005X40N                   BottomLayer 270      
1966.732      564.173       SAMTEC_SEAM-40-03.5-S-08-2-A-K ""                        J41        SAMTEC_SEAM-40-03.5-S-08-2-A-K BottomLayer 180      
1818.630      1914.638      10nF                           "Non-polarised capacitor" C9         CAPC1005X55N                   BottomLayer 135      
1709.646      1856.299      10nF                           "Non-polarised capacitor" C8         CAPC1005X55N                   TopLayer    360      
1820.866      1998.032      10nF                           "Non-polarised capacitor" C7         CAPC1005X55N                   BottomLayer 135      
1739.173      1963.583      10nF                           "Non-polarised capacitor" C6         CAPC1005X55N                   TopLayer    360      
1811.024      2066.929      10nF                           "Non-polarised capacitor" C5         CAPC1005X55N                   BottomLayer 135      
1016.693      1314.173      10nF                           "Non-polarised capacitor" C1         CAPC1005X55N                   BottomLayer 270      
1767.716      2073.819      10nF                           "Non-polarised capacitor" C4         CAPC1005X55N                   TopLayer    360      
1116.693      1314.173      10nF                           "Non-polarised capacitor" C3         CAPC1005X55N                   BottomLayer 270      
1066.693      1314.173      10nF                           "Non-polarised capacitor" C2         CAPC1005X55N                   BottomLayer 270      
2247.943      1570.423      22k                            "Resistor - 1%"           R46        RESC1005X40N                   TopLayer    135      
1891.693      1170.669      0                              Resistor                  R1         RESC1005X40N                   TopLayer    90       
1704.193      1170.669      0                              Resistor                  R2         RESC1005X40N                   TopLayer    90       
1641.693      1170.669      0                              Resistor                  R3         RESC1005X40N                   TopLayer    90       
1516.693      1170.669      0                              Resistor                  R4         RESC1005X40N                   TopLayer    90       
1829.193      1170.669      0                              Resistor                  R5         RESC1005X40N                   TopLayer    90       
1766.693      1170.669      0                              Resistor                  R6         RESC1005X40N                   TopLayer    90       
1266.693      945.669       0                              Resistor                  R8         RESC1005X40N                   TopLayer    90       
